## About
The UXR_Insights repository is the single source of truth (SSOT) for all user insights discovered by GitLab’s UX Researchers and UX/Product Designers. User insights can be gathered in a number of different ways including, but not limited to, user interviews, usability testing, surveys, card sorts, tree tests and diary studies.

## Issues
Instead of reports and slide decks, we use issues to document key findings from research studies. Every issue within the UXR_Insights repository contains a single insight on a particular topic. Each insight is supported with evidence, typically in the form of a video clip or statistical data. We use GitLab’s related issues functionality to connect insights which were revealed during the same research study. We close issues when we have finished editing them.

## Labels
We use labels to tag insights. They allow us to quickly and dynamically search and filter through issues to find the insights we need. As a rule of thumb, we generally tag labels with the relevant stage group (~"Plan",~"Secure", etc) research type (see below) and the area/feature (~"navigation", ~"merge requests", etc) of GitLab the insight relates to as a bare minimum.

##### Research Types
~"usability testing uxr" 
~"survey uxr"
~"user interview uxr"
~"design evaluation uxr"
~"card sorting uxr"
~"tree testing uxr"
~"competitor analysis uxr"

## Epics
We use Epics to track issues from the same research study. The Epic description usually contains the research methodology used, and if we spoke with users, then we’ll include any background information we have about them. We tag all Epics with the ~"uxr_insights" label so they are easily found within the GitLab.org group.

## Directory of completed research

##### Auto DevOps

2018-05 [Discoverability of Auto DevOps - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1210)

2018-01 [Reduce the installation time for Kubernetes - Internal Demo](https://gitlab.com/gitlab-org/uxr_insights/issues/271)

##### CI/CD
2019-06 [CI/CD - How to refer to a release that is not ready for production](https://gitlab.com/gitlab-org/uxr_insights/issues/482)

2019-04 [Cross-Project, "X" MVC: Feature Naming - Design Evaluation](https://gitlab.com/groups/gitlab-org/-/epics/1441)

2018-05 [CI/CD Adoption & Usage - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1221)

##### Code Review & Merge Requests
2019-10 [File-by-File Diff Mode - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/2070)

2019-09 [Git Icon Testing - Out-of-Context](https://gitlab.com/groups/gitlab-org/-/epics/1959)

2019-04 [Code Review improvements - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1259)

2017-10 [Distinguishing between MRs and Issues - Click Test](https://gitlab.com/groups/gitlab-org/-/epics/1267)

2017-07 [Merge Request page - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1274)

##### Dashboards

2018-11 [Operations Dashboard - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1234)

##### Developer Survey Report 
2019-02 [Developer Survey Report Insights - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1841)

##### General
2018-12 [General customer feedback - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1220)

2017-08 [Managing workflow - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1209)

##### GitLab.com

2018-06 [GitLab.com SAML - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1448)

2018-09 [Evaluating GitLab.com - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1230)

2018-07 [GitLab.com user - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1229)

##### Groups

2019-02 [Group overview page - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1203)

2018-08 [Group list - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1215)

##### Issues
2019-06 [Issue Board Capacity Line - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1578)

2019-05 [Issues Right Sidebar Usage & Preferences - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1411)

2019-02 [Closed issue icon - Design Evaluation](https://gitlab.com/groups/gitlab-org/-/epics/1286)

2018-11 [Images in issue boards - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1218)

2017-11 [Issue display and sort order - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1233)

2017-10 [Moving an issue between projects - Click Test](https://gitlab.com/groups/gitlab-org/-/epics/1265)

2017-03 [Issue improvements - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1273)

##### Labels
2019-08 [Labels in Boards - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1852)

##### Navigation
2019-06 [Q2-2019 Research Initiative: Stakeholder Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1561)

2018-12 [Sidebar navigation - Card Sort](https://gitlab.com/groups/gitlab-org/-/epics/1235)

2018-12 [Sidebar navigation - Tree Testing](https://gitlab.com/groups/gitlab-org/-/epics/1236)

2018-11 [Benchmarking sidebar navigation - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1213)

2018-09 [Interacting with GitLab Instance - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1216)

2017-12 [Contextual Navigation - Tree Test](https://gitlab.com/groups/gitlab-org/-/epics/1447)

2017-11 [Contextual Navigation - Card Sort](https://gitlab.com/groups/gitlab-org/-/epics/1446)

2017-10 [Navigation sidebar colour - Click Test](https://gitlab.com/groups/gitlab-org/-/epics/1268)

2017-04 [Navigation Round 3 - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1412)

2017-04 [Navigation Round 2 - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1208)

2017-03 [Navigation Round 1 - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1358)

##### Onboarding

2019-04 [User onboarding prototypes - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1205)

2018-05 [Configuring SSH - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1227)

##### Permissions & Roles

2018-06 [Permissions & Roles - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1223)

##### Personas
2019-05 [GitLab Data Analysts - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1420)

2019-02 [Product Designers - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1331)

2018-09 [Security Aware Developers - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1272)

2018-09 [Site Reliability Engineers - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1271)

2018-09 [DevOps Engineers - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1270)

2018-09 [Software Engineers - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1269)

2018-09 [Product Managers - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1364)

2018-09 [Development Team Leads - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1365)

2018-03 [Operations Engineers - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1232)

2018-03 [Operations Engineers - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1212)

##### Project Page/Settings/List

2018-10 [Admin & Project Settings - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1222)

2018-08 [Project list - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1214)

2018-07 [Project page - Click Test](https://gitlab.com/groups/gitlab-org/-/epics/1226)

2017-10 [Project page design - Click Test](https://gitlab.com/groups/gitlab-org/-/epics/1266)

#####  Secure

2019-08 [SAST setup - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1784) 

2019-07 [Stakeholder Interviews](https://gitlab.com/groups/gitlab-org/-/epics/1829) 

2019-07 [Dependency list - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1800)  

2018-09 [License Management - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1225)  

##### Serverless

2018-11 [Serverless - Survey](https://gitlab.com/groups/gitlab-org/-/epics/1224)

##### Snippets
2019-10 [Snippets - User Interviews](https://gitlab.com/groups/gitlab-org/-/epics/2030)

##### System Usability Scale (SUS) Survey
Q2-2019 [System Usability Scale (SUS) Survey Q2 2019 ](https://gitlab.com/groups/gitlab-org/-/epics/1622)

Q1-2019 [System Usability Scale (SUS) Survey Q1 2019](https://gitlab.com/groups/gitlab-org/-/epics/1455)

##### Web IDE

2019-07 [Web IDE - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1579)

2018-12 [Web IDE vs Ace Editor - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1219)

##### GitLab Commit events

2019-10 [Commit London event 2019 - findings and recommendations](https://gitlab.com/gitlab-org/uxr_insights/issues/646)

## F.A.Q.s

### How do you recruit users for research? / How can I participate in research?
Please join our research panel: [GitLab First Look](https://about.gitlab.com/community/gitlab-first-look/).

### How can I request research?
Please refer to the [guidance in the handbook](https://about.gitlab.com/handbook/engineering/ux/ux-research/#how-to-request-research).


##  Contributing
GitLab is an open source project and we are very happy to accept community contributions. Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.

Please note this project is used for issue tracking only.


## Project Members
Owner: @sarahod

Maintainers: @katokpara, @tlavi, @loriewhitaker

## Related Links
[UX Research project](https://gitlab.com/gitlab-org/ux-research)

[UX Research handbook](https://about.gitlab.com/handbook/engineering/ux/ux-research/)
