### State of Play: An overview of GitLab’s navigation UX research 
As a research team, we are always concerned with investigating and advocating for the needs of all GitLab users. We have a long history of research that has culminated in incremental improvements to GitLab’s UI over time. Before we outline our future research and design plans, let’s go through an overview of our navigation-related research studies and key findings.

If you’d like to read more about our navigation research, please visit the [UX research insights repository](https://gitlab.com/gitlab-org/uxr_insights#navigation). 

#### Early usability testing that informed GitLab’s 2017 navigation redesign efforts
2017-03 [Navigation Round 1 - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1358)
- Research Objective: This first round of testing explored the usability of GitLab’s project navigation, group navigation, left sidebar, global navigation, and search. The objective of this research was to get a general understanding of the way that users navigate throughout GitLab, while uncovering any usability issues.
- Methodology: Moderated usability testing with 6 GitLab users. Users were asked to complete tasks using GitLab’s existing features.
- Conclusion: One of the major issues discovered in this round of testing was confusion between global content vs. contextual content for new users when navigating. This confusion caused difficulty in trying to find projects, distinguishing between personal projects and other memberships, and understanding where you were in GitLab. The majority of changes to the navigation design in 9.0 did not address the concerns highlighted by this round of testing.

2017-04 [Navigation Round 2 - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1208)
- Research Objective: Conducted as a follow-up to round 1. This research [revisited unresolved questions](https://gitlab.com/gitlab-org/ux-research/issues/5#progress-since-last-usability-testing-session-3 from the first round of testing and also [evaluated icon recognition / functionality](https://gitlab.com/gitlab-org/ux-research/issues/5#icon-recognition-and-functionality-out-of-context) in a wide variety of product areas, such as groups, Issues, project creation, and project settings. 
- Methodology: Moderated usability testing of the user’s own GitLab instance, with 6 GitLab users. Icon testing inside and outside of context.
- Conclusion: This study resulted in a [list of usability issues and recommendations](https://gitlab.com/gitlab-org/ux-research/issues/5#usability-issues-and-recommendations) ranked by priority. In terms of progress since the last session, all users were able to find their personal projects, the majority of users understood the context of issues and MR icons, and no users manually counted their projects (since the badge count was added to project, group, and subgroup tabs).

2017-04 [Navigation Round 3 - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1412)
- Research Objective: Conducted as a follow-up to round 2. 
- Methodology:  Moderated usability testing of 2 prototypes - [A](https://framer.cloud/fyEvF/index.html) and [B](https://framer.cloud/jveno/index.html). Each prototype was tested with 6 GitLab users.
- Conclusion: This study resulted in a [list of usability issues and recommendations](https://gitlab.com/gitlab-org/ux-research/issues/7#usability-issues-and-recommendations) ranked by priority. In terms of progress since the last session, the global navigation generally tested well with all users, almost all users understood what the label of 'owner' meant on the 'Your Projects' screen, users utilized tooltips to identify icons, and feedback was more positive or neutral.

#### Improvements to GitLab’s left sidebar 
This research came after significant improvements had already been made to the navigation and the UX team was continuing to build on that momentum. After addressing issues with the placement of global and contextual elements, most of our research began to focus on the information architecture of the sidebars, especially the left one. This is in part because of the fact that the left sidebar became the “go-to” place for adding links to new features and was quickly becoming more impacted than before.

2017-10 [Navigation sidebar color - Click Test](https://gitlab.com/groups/gitlab-org/-/epics/1268)
- Research Objective: The purpose of this research was to assess what kind of impact changing the color of the right sidebar would have on a user's ability to find content located in the sidebar, if any. The goal for this design was to make the issues sidebar feels like part of the issues page but still be perceived as a distinguishable action area for the issue.
- Methodology: Usability click test with 144 GitLab and non GitLab users. The click test consisted of two variants: a) existing sidebar and b) white sidebar. Users were asked where they would click to add a due date to an issue. Each user saw one of the variants. 
- Conclusion: The white sidebar performed worse than the existing sidebar color. Users had a higher propensity to click the ‘Edit’ CTA on the issue description when they were shown the white sidebar, suggesting that users were less likely to align the sidebar content with the page content. As a result of this research, the [proposal to make the right sidebar white](https://gitlab.com/gitlab-org/gitlab-ce/issues/35740) was closed. 

2017-12 [Project & Group Level Card Sorts](https://gitlab.com/gitlab-org/ux-research/issues/35)
- Research Objective: Evaluating the information architecture of the left sidebar, in order to better understand how users categorize GitLab features and align the content more closely with user expectations.
- Methodology: Open card-sort of contextual navigation items. Users were asked to order content within the categories.
- Conclusion: This research resulted in a recommended structure for the contextual navigation at a project and group level. This included recommendations such as changing the “Analytics” category to “Statistics” at the group level, moving “Contributor Analytics” under the “Statistics” category at the group level, moving “Details” under “Project” at the project level, and more. 

2018-03  [Project & Group Level Tree Testing](https://gitlab.com/gitlab-org/ux-research/issues/43)
- Research Objective: Assessing the amount of an improvement shown by using the recommended structure for the contextual navigation that was created based on a card-sorting study. Understanding whether the differences in how content is organized at a project and a group level causes users confusion.
- Methodology: Tree-testing the existing contextual navigation at a project and group level. Testing the recommended structure for the contextual navigation at a project and group level.
- Conclusion: This research resulted in suggested, iterative changes that were recommended to be made to the sidebar. The results of the tree-test showed that a hybrid of the existing and proposed structure worked best and the recommendations outlined context for the proposed changes.

#### Evaluating the proposal to organize GitLab’s left sidebar by the 9 DevOps stages
In October 2018, a [research proposal](https://gitlab.com/gitlab-org/ux-research/issues/108) was opened, asking the question of how to better organize the links the left sidebar. Additionally, this research proposal suggested that we should organize the left sidebar by GitLab’s 9 DevOps stages, in order to align with the way our company was being structured. 

Three rounds of research were conducted in order to benchmark the usability of the left sidebar, determine whether users were familiar with GitLab’s DevOps stages, and evaluate the findability of items (when presented with stage names as top-level categories). We concluded that this series of research did not support ordering the left sidebar with GitLab's 9 DevOps stages as top-level items due to how the results compared to previous tests, usability concerns, and feedback from users.

2018-11 [Benchmarking sidebar navigation - Usability Testing](https://gitlab.com/groups/gitlab-org/-/epics/1213)
- Research Objective: Understand user experiences navigating throughout GitLab. Understand the cold and hot spots of the left side navigation. Understand what should be top-level, flattened, added, removed, etc.
- Methodology: Conduct usability benchmarking on the existing left side navigation via user's own GitLab instance. Testing occurred with GitLab.com users, with the exception of User 5. User 5 completed Task 15 in the same way as other users but was shown a mockup from GitLab documentation, in order to evaluate the Operations Dashboard icon.
- Conclusion: 
   - Feature recognition: Some users are aware of the concept of a feature but do not know the exact name of it. They may be more oriented towards looking for it under a “section” (such as CI/CD or Issues) even if they can't pinpoint it. Other users remember feature names very well (e.g. heard about it in a blog post, release update, etc) but aren’t aware of how to actually find that feature. 
   - Context: Many users have multiple groups set up across their instance to manage projects, clients, applications, and more. Due to this, some users lost context for certain features because they were only enabled in a certain project. They’d have to navigate to the right project before finding the feature available. Users were immediately able to recognize that they were in the wrong project for tasks related to CI/CD and Issues but had less memory of these differences when it came to other features.

2018-12 [Sidebar navigation - Card Sort](https://gitlab.com/groups/gitlab-org/-/epics/1235)
- Research Objective: As we continued to discuss the proposal to reorder the left sidebar by GitLab's 9 DevOps stages (#108), we decided to conduct further research to better understand how users categorize GitLab features.
- Methodology: Open card-sort of the existing project-level left sidebar features (user create their own categories). Hybrid card-sort for the project-level left sidebar features (users are asked to sort features into the 9 DevOps stages, but they can also create their own categories).
- Conclusion: 
   - Open card-sort: There was a lot of disagreement over how to categorize left sidebar items. The smallest amount of categories a card was sorted into was 17, the largest amount was 28. Despite variations in what the categories were named, many participants created categories with similar concepts such as "Dev" or "Source" or "Code" for items related to the current Repository left sidenav item.
   - Hybrid card-sort: There was a lot of disagreement over where left sidebar items should go. The smallest amount of categories a card was sorted into was 4, the largest amount was 11. The categories with the highest level of agreement were “Create” (26% agreement), Package (25% agreement), “Plan” (22% agreement) The categories with the lowest level of agreement were “Configure” (13% agreement), “Manage” (14% agreement), “Verify” (16% agreement). 50% of participants had some level of familiarity with GitLab’s 9 DevOps stages.

2018-12 [Sidebar navigation - Tree Testing](https://gitlab.com/groups/gitlab-org/-/epics/1236)
- Research Objective: The objective of this research was to better understand how users understand GitLab's product offerings and evaluate the findability of the proposed navigation structure.
- Methodology: Tree Testing of a left sidebar organized by DevOps stages via Optimal Workshop.
- Conclusion: While it may be possible to teach users about GitLab’s 9 DevOps stages and have them learn to use the updated left sidebar, the findings and feedback from users show that the layers of sub-navigation would ultimately make navigation a noisier and more cumbersome experience.
   - Instead of just understanding the feature of “Issues Tracker,” users would need to process a high-level category such as “Plan” or “Project Management” and then drill-down to the exact feature they desire. This is especially concerning due to the fact that not all users were aware of GitLab’s 9 DevOps stages.
   - Additionally, a few of the names of the 9 DevOps stages are not immediately descriptive (e.g. “Manage,” “Create,” “Configure”) and may require a bit more guesswork to understand the variety of features that could fall under there.

2019-03 [System Usability Scale](https://gitlab.com/gitlab-org/uxr_insights/issues/492)
In the 1st quarter for 2019, we started running a quarterly survey to assess the System Usability Score for each stage group. This survey provided insight into how users interact with features in all stages of the product. 8 users provided open text feedback about navigation and discoverability in GitLab. 
