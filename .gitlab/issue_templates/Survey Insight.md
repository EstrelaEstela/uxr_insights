<!---
Please read this!

This template is designed to help you add an insight to the UXR_Insights Repository.

1. Issue title
    - Please use a descriptive title for your insight in order to aid findability.
    - Start your title with the feature, persona or area of GitLab your insight relates to.
    - Followed by a brief description of the insight, problem or research question your insight addresses.
    
    For example:
        Auto DevOps - Failed to understand the GCP credit banner
        Operation Engineers - What do users want from a monitoring solution?
        Configuring SSH - Locating an existing key pair

2. Issue content
    - One insight per issue.
    - The insight can relate to multiple users.
    - Please create a new issue for each additional insight you have.
    - All fields are mandatory. 
    - Please do not leave fields blank or submit insights without video evidence.

3. Labels
    - Please ensure you add the relevant `feature` and `stage group` and/or `persona` labels to your insight.

4. Assignee
    - Please assign the issue to yourself.

5. Issue status
    - When you have finished editing the issue, please close the issue.

6. Related issues
    - Please add issues/insights revealed during the same research study to related issues.

7. Epic
    - Every insight issue should belong to an Epic.
    - Please use a descriptive title for your Epic in order to aid findability.
    - Start your title with the feature, persona or area of GitLab your Epic relates to.
    - Followed by the testing methodology and date the research was completed.

    For example:
        Operations Engineers - Survey - May 2019

    - Add the `uxr_insights` label to your Epic.
    - Close the Epic.
    - Add a link to your Epic in UXR_Insights Respository's ReadMe file under the `Directory of completed research`

8. Any questions? 
    - Please reach out to a member of the UX Research team:
        Sarah O'Donnell
        Katherine Okpara
        Tali Lavi
        Lorie Whitaker
--->

## Survey question

<!---What question did you ask users?--->

## Results

<!---What were the results to the question?--->

## Insight

<!---What have you found out?--->

## Date research undertaken

<!---When did you discover this insight? Please enter the month and year--->

/label ~"survey uxr" 
